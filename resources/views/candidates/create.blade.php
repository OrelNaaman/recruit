@extends('layouts.app')
@section('title','Create candidate')
@section('content')
    <body>
        <h1>Create Candidates</h1>
        @if (count($errors) > 0)
        <div class="alert alert -danger">
            <ul>
                @foreach ($errors -> all() as $error)
                    <li>{{$error}}</li>    
                @endforeach
            </ul>
        @endif
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf 
        <div>
            <label for = "name">Candidate name</label>
            <input type = "text" name = "name">
        </div>
        <div>
            <label for = "email">Candidate email</label>
            <input type = "text" name = "email">
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Create candidate">
        </div>
        </form>      
    </body>
@endsection