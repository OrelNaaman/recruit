@extends('layouts.app')
@section('content')
    <div><a class="badge badge-primary text-wrap" href="{{url('/candidates/create')}}">Add new candidate</a></div>
    </div>
    <h1>List of candidates</h1>
    @if ($message = Session::get('success'))
    <div class="alert alert -success">
    <p>{{$message}}</p>
    </div>
    @endif
    <table class = "table table-dark">
        <tr>
            <th>id</th><th>Name</th><th>Email</th><th>Status</th><th>Owner</th><th>Edit</th><th>Created</th><th>Updated</th><th>Delete</th>
        </tr>
        <!-- tha table data -->
        @foreach($candidates as $candidate)
        <tr>
            <td>{{$candidate -> id}}</td>
            <td>{{$candidate -> name}}</td>
            <td>{{$candidate -> email}}</td>
            <td>
            <div class="dropdown">
                @if (null != App\Status::next($candidate->status_id)) 
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 @if(isset($candidate->status_id))
                    {{$candidate->situation->name}}
                 @else
                 Assign Status
                 @endif
                </button>
                @else 
                {{$candidate->situation->name}}
                @endif
                @if(App\Status::next($candidate->status_id)!=null)
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach(App\Status::next($candidate->status_id) as $status)
                <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id,$status->id])}}">{{$status->name}}</a>
                @endforeach
                </div>
                @endif
                </div>
                </td>
            </td>
            <td>
                <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 @if(isset($candidate->user_id))
                    {{$candidate->owner->name}}
                 @else
                 Assign Owner
                 @endif
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($users as $user)
                <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id,$user->id])}}">{{$user->name}}</a>
                @endforeach
                </div>
                </div>
                </td>
            <td>{{$candidate -> created_at}}</td>
            <td>{{$candidate -> updated_at}}</td>
            <td> <a class="btn btn-primary" href="{{route('candidates.edit',$candidate->id)}}">Edit </a></td>
            <td> <a class="btn btn-primary" href="{{route('candidate.delete',$candidate->id)}}">Delete </a></td>
        </tr>
        @endforeach
    </table>
@endsection